package com.rpn;

import org.junit.Test;

import java.math.BigDecimal;

import static org.junit.Assert.*;

public class CalculatorTest {


    @Test
    public void plus() {
        Calculator calc = new Calculator();

        // Empty stack
        boolean isSuccessful = calc.plus();
        assertFalse(isSuccessful);

        // One operand
        calc.pushInStack(new BigDecimal("1.5"));
        isSuccessful = calc.plus();
        assertFalse(isSuccessful);

        // Two operands
        calc.clear();
        calc.pushInStack(new BigDecimal("1.5"));
        calc.pushInStack(new BigDecimal("2"));
        isSuccessful = calc.plus();
        assertTrue(isSuccessful);
        assertEquals(1, calc.getStack().size());
        assertEquals(0, new BigDecimal("3.5").compareTo(calc.getStack().pop()));

    }

    @Test
    public void minus() {
        Calculator calc = new Calculator();

        // Empty stack
        boolean isSuccessful = calc.minus();
        assertFalse(isSuccessful);

        // One operand
        calc.pushInStack(new BigDecimal("3"));
        isSuccessful = calc.minus();
        assertFalse(isSuccessful);

        // Two operands
        calc.clear();
        calc.pushInStack(new BigDecimal("1.5"));
        calc.pushInStack(new BigDecimal("2"));
        isSuccessful = calc.minus();
        assertTrue(isSuccessful);
        assertEquals(1, calc.getStack().size());
        assertEquals(0, new BigDecimal("-0.5").compareTo(calc.getStack().pop()));
    }

    @Test
    public void multiply() {
        Calculator calc = new Calculator();

        // Empty stack
        boolean isSuccessful = calc.multiply();
        assertFalse(isSuccessful);

        // One operand
        calc.pushInStack(new BigDecimal("1.5"));
        isSuccessful = calc.multiply();
        assertFalse(isSuccessful);

        // Two operands
        calc.clear();
        calc.pushInStack(new BigDecimal("1.5"));
        calc.pushInStack(new BigDecimal("2"));
        isSuccessful = calc.multiply();
        assertTrue(isSuccessful);
        assertEquals(1, calc.getStack().size());
        assertEquals(0, new BigDecimal("3").compareTo(calc.getStack().pop()));
    }

    @Test
    public void divide() {
        Calculator calc = new Calculator();

        // Empty stack
        boolean isSuccessful = calc.divide();
        assertFalse(isSuccessful);

        // One operand
        calc.pushInStack(new BigDecimal("1.5"));
        isSuccessful = calc.divide();
        assertFalse(isSuccessful);

        // divide by zero
        calc.clear();
        calc.pushInStack(new BigDecimal("3"));
        calc.pushInStack(BigDecimal.ZERO);
        isSuccessful = calc.divide();
        assertTrue(isSuccessful);
        assertTrue(calc.getStack().isEmpty());

        // A valid division
        calc.clear();
        calc.pushInStack(new BigDecimal("7"));
        calc.pushInStack(new BigDecimal("3"));
        isSuccessful = calc.divide();
        assertTrue(isSuccessful);

        // Verify default scale value: 15
        assertEquals(0, new BigDecimal("2.333333333333333").compareTo(calc.getStack().pop()));
    }

    @Test
    public void squareRoot() {
        Calculator calc = new Calculator();

        // Empty stack
        boolean isSuccessful = calc.squareRoot();
        assertFalse(isSuccessful);

        // Negative value
        calc.clear();
        calc.pushInStack(new BigDecimal("-3"));
        isSuccessful = calc.squareRoot();
        assertFalse(isSuccessful);
        assertTrue(calc.getStack().isEmpty());

        // A valid operation
        calc.clear();
        calc.pushInStack(new BigDecimal("3"));
        isSuccessful = calc.squareRoot();
        assertTrue(isSuccessful);
        assertEquals(1, calc.getStack().size());
    }
}