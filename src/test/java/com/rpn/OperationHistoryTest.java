package com.rpn;

import org.junit.Test;

import java.math.BigDecimal;
import java.util.Stack;

import static org.junit.Assert.*;

public class OperationHistoryTest {


    @Test
    public void testHistorySize() {
        OperationHistory history = new OperationHistory();

        // Pop from empty history
        Stack s = history.pop();
        assertNull(s);

        // Push one item and try popping two
        history.push(new Stack<BigDecimal>());

        s = history.pop();
        assertNotNull(s);

        s = history.pop();
        assertNull(s);

        //Push 3 items. Only 2 items should be there
        history.push(new Stack<BigDecimal>());
        history.push(new Stack<BigDecimal>());
        history.push(new Stack<BigDecimal>());

        s = history.pop();
        assertNotNull(s);

        s = history.pop();
        assertNotNull(s);

        s = history.pop();
        assertNull(s);

    }

    @Test
    public void testHistoryContent() {

        // prepare data
        Stack<BigDecimal> s1 = new Stack<>();
        s1.push(new BigDecimal("1"));

        Stack<BigDecimal> s2 = new Stack<>();
        s2.push(new BigDecimal("1"));
        s2.push(new BigDecimal("2"));

        Stack<BigDecimal> s3 = new Stack<>();
        s3.push(new BigDecimal("1"));
        s3.push(new BigDecimal("2"));
        s3.push(new BigDecimal("3"));

        // push into history
        OperationHistory history = new OperationHistory();
        history.push(s1);
        history.push(s2);
        history.push(s3);

        // verify history. Should contain 3, 2, 1
        Stack<BigDecimal> s = history.pop();
        assertEquals(3, s.size());
        assertEquals(3, s.pop().intValue());
        assertEquals(2, s.pop().intValue());
        assertEquals(1, s.pop().intValue());

        // Should contain 2, 1
        s = history.pop();
        assertEquals(2, s.size());
        assertEquals(2, s.pop().intValue());
        assertEquals(1, s.pop().intValue());

        // History history should be empty
        s = history.pop();
        assertNull(s);
    }
}