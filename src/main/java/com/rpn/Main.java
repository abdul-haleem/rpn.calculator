package com.rpn;

import java.math.BigDecimal;
import java.util.Scanner;

/**
 * Main class implementing Read, Evaluate, Print loop.
 */
public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);


        System.out.println("RPN Calculator. Start input:");
        Calculator calc = new Calculator();
        while(true) {
            String input[] = scanner.nextLine().split("\\s");

            boolean isSuccessful = true;
            for (int i = 0; i < input.length && isSuccessful; i++) {
                String data = input[i];

                switch (data) {
                    case "+":
                        isSuccessful = calc.plus();
                        break;
                    case "-":
                        isSuccessful = calc.minus();
                        break;
                    case "*":
                        isSuccessful = calc.multiply();
                        break;
                    case "/":
                        isSuccessful = calc.divide();
                        break;
                    case "sqrt":
                        isSuccessful = calc.squareRoot();
                        break;
                    case "undo":
                       calc.undo();
                        break;
                    case "clear":
                        calc.clear();
                        break;
                    case "exit":
                        return;
                    default:
                        try {
                            BigDecimal number = new BigDecimal(data);
                            calc.pushInStackWithHistory(number);
                        } catch (NumberFormatException e) {
                            isSuccessful = false;
                        }

                }
                if (!isSuccessful) {
                    calc.printInvalidParamMessage(data, i+1);
                }
            }
            calc.printStack();
        }
    }

}
