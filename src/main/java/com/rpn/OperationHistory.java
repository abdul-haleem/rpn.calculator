package com.rpn;

import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.util.Deque;
import java.util.LinkedList;
import java.util.Stack;

/**
 * The class implementing history for undo operation.
 */
public class OperationHistory {

    private Config conf = ConfigFactory.load();
    private int historySize;
    private Deque<Stack<BigDecimal>> history = new LinkedList<>();
    private static Logger logger = LoggerFactory.getLogger(OperationHistory.class);

    public OperationHistory() {
        historySize = conf.getInt("rpn.calculator.history.size");
    }

    /**
     * Pushes current stack to history.
     * @param data
     */
    public void push(Stack<BigDecimal> data) {
        if (history.size() == historySize) {
            history.removeLast();
        }

        history.addFirst((Stack<BigDecimal>)data.clone());
        logger.debug("New entry in history: " + data.toString());
    }

    /**
     * Pops out stacks last state.
     * @return
     */
    public Stack<BigDecimal> pop() {
        if (history.size() > 0) {
            return history.removeFirst();
        }

        return null;
    }

    /**
     * Clears the history.
     */
    public void clear() {
        history.clear();
    }
}
