package com.rpn;

import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.Arrays;
import java.util.Stack;

/**
 * Implements basic arithmetic operations on real numbers.
 */
public class Calculator {
    private BigDecimal left, right;
    private Stack<BigDecimal> stack = new Stack<>();
    private OperationHistory operationHistory = new OperationHistory();
    Config config = ConfigFactory.load();
    int divisionScale;
    int displayScale;

    private static Logger logger = LoggerFactory.getLogger(Calculator.class);

    public Calculator() {
        divisionScale = config.getInt("rpn.calculator.scale.division");
        displayScale = config.getInt("rpn.calculator.scale.display");
    }

    /**
     * Verifies number of operands and pops them out.
     * @return true if number of operands is verified, false otherwise.
     */
    private boolean verifySizeAndExtractOperands() {
        if (stack.size() < 2) {
            logger.debug("Insufficient operands");
            return false;
        }

        operationHistory.push(stack);
        right = stack.pop();
        left = stack.pop();

        return true;
    }

    public Stack<BigDecimal> getStack() {
        return stack;
    }

    /**
     * Pushes numbers in stack and history.
     * @param data
     */
    public void pushInStackWithHistory(BigDecimal data) {
        if (!stack.isEmpty()) {
            operationHistory.push(stack);
        }
        logger.debug("Pushing " + data + " to stack");
        stack.push(data);
    }

    /**
     * Pushes number in stack.
     * @param data
     */
    public void pushInStack(BigDecimal data) {
        logger.debug("Pushing " + data + " to stack");
        stack.push(data);
    }

    /**
     * Tries to pop operands from stack, adds them and pushes the result back.
     * @return true if successful, false otherwise
     */
    public boolean plus() {
        if(!verifySizeAndExtractOperands()) {
            return false;
        }

        logger.info("Performing " + left + " + " + right);
        pushInStack(left.add(right));
        return true;
    }

    /**
     * Tries to pop operands from stack, performs subtraction and pushes the result back.
     * @return true if successful, false otherwise
     */
    public boolean minus() {
        if(!verifySizeAndExtractOperands()) {
            return false;
        }

        logger.info("Performing " + left + " - " + right);
        pushInStack(left.subtract(right));
        return true;
    }

    /**
     * Tries to pop operands from stack, performs multiplication and pushes the result back.
     * @return true if successful, false otherwise
     */
    public boolean multiply() {
        if(!verifySizeAndExtractOperands()) {
            return false;
        }

        logger.info("Performing " + left + " x " + right);
        pushInStack(left.multiply(right));
        return true;
    }

    /**
     * Tries to pop operands from stack, performs division and pushes the result back.
     * @return true if successful, false otherwise
     */
    public boolean divide() {
        if(!verifySizeAndExtractOperands()) {
            return false;
        }

        try {
            logger.info("Performing " + left + " / " + right);
            pushInStack(left.divide(right, divisionScale, RoundingMode.HALF_UP));
        } catch (ArithmeticException e) {
            System.out.println("Error while dividing " + left + " by " + right);
        }
        return true;

    }

    /**
     * Tries to pop operand from stack, performs square root and pushes the result back.
     * @return true if successful, false otherwise
     */
    public boolean squareRoot() {
        if (stack.size() < 1) {
            return false;
        }
        operationHistory.push(stack);
        Double data = stack.pop().doubleValue();
        logger.info("Performing SQRT " + data);
        Double result = Math.sqrt(data);

        // if sqrt is performed on a negative number
        if (result.isNaN()) {
            return false;
        }

        pushInStack(BigDecimal.valueOf(result));
        return true;
    }

    /**
     * Clears stack and history.
     */
    public void clear() {
        logger.info("Clearing stack and history");
        stack.clear();
        operationHistory.clear();
    }

    /**
     * Undo last action.
     */
    public void undo() {
        logger.info("Undoing last action");
        Stack<BigDecimal> undoneStack = operationHistory.pop();
        if (undoneStack != null) {
            stack = undoneStack;
        } else {
            String msg = "Limit reached. Cannot undo further";
            logger.info(msg);
            System.out.println(msg);
        }
    }

    /**
     * Prints stack to console.
     */
    public void printStack() {
        // show up to 10 decimal places
        char[] patternChar = new char[displayScale];
        Arrays.fill(patternChar,'#');
        String pattern = ".".concat(new String(patternChar));

        DecimalFormat decimalFormat = new DecimalFormat(pattern);
        System.out.print("Stack: ");
        stack.stream().forEach(d -> System.out.print(decimalFormat.format(d) + "  "));
        System.out.println();
    }

    /**
     * Prints error message to console.
     * @param data
     * @param position
     */
    public void printInvalidParamMessage(String data, int position) {
        switch (data) {
            case "sqrt":
                System.out.println("Operator " + data + " (position: " + position + "): invalid parameter");
                break;
            case "+":
            case "-":
            case "*":
            case "/":
                System.out.println("Operator " + data + " (position: " + position + "): insufficient parameters");
                break;
            default:
                System.out.println("Data " + data + " (position: " + position + "): invalid format");

        }
    }
}
