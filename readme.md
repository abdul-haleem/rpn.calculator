# Introduction

RPN Calculator is a console based maven project. It provides basic arithmetic operations on real numbers.

# Usage

Calculator can be launched in two ways:
1. Open the project in any Java IDE and run `main` method in `Main.java`.
2. Use `mvn clean test install` command to build the project. It will create a single jar with all dependencies under `target` folder named `service.calculator-1.0-SNAPSHOT-jar-with-dependencies.jar`. Use `java -jar service.calculator-1.0-SNAPSHOT-jar-with-dependencies.jar` to launch the program.

The calculator waits for user input. You can enter space separated real numbers and operators. Upon pressing enter key the calculator starts reading from left. If a number is found it is pushed to stack. Upon reading an operator, required number of operands are popoed from stack, operation is performed and result is pushed back.
  
# Supported Operations

Following operations are supported

## + (Addition)
### Example
Input: 1 2 +
Stack: 3

Input: 3 4 5.5 +
Stack: 3 9.5

## - (Subtraction)
### Example
Input: 5 8 -
Stack: -3

## * (Multiplication)
### Example
Input: 3 9 *
Stack: 27

## / (Division)
Result of division is saved upto decimal places defined by `rpn.calculator.scale.division` environment variable. Default value is 15. 
### Example
Input: 5 2 /
Stack: 2.5

## sqrt (Square root)
### Example
Input: 25 sqrt
Stack 5

## undo
Undos last action on stack. Number of operations the calculator can undo is defined by `rpn.calculator.history.size`  environment variable. Default value is 2. Further undos will have no effect.
### Example
input: 1 4 2 undo
Stack: 1 4

input: 3 2 5 +
Stack: 3 7
Input: undo
Stack: 3 2 5

## clear
clears calculator stack and history.

## exit
exits calculator program.

## Further Examples
Input: 3 2 5 + -
Stack: -4
Input: undo
Stack: 3 7
Input: clear
Stack: 
Input: 10 2 / 3 -
Stack: 2.0  
Input: undo
Stack: 5.0  3.0  
Input: undo
Stack: 5.0  
Input: undo
Limit reached. Cannot undo further
Stack: 5.0  